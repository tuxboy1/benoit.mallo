user := $(shell id -u)
group := $(shell id -g)
dc := USER_ID=$(user) GROUP_ID=$(group) docker-compose
sy := $(dc) exec php php ./bin/console

.PHONY: dev
dev: vendor/autoload.php
	$(dc) up

.PHONY: stop
stop:
	$(dc) down

.PHONY: php
php:
	$(dc) exec php bash

.PHONY: lint
lint: vendor/autoload.php
	# docker run -v $(PWD):/app -w /app -t --rm php:8.0-cli-alpine php -d memory_limit=-1 bin/console lint:container
	docker run -v $(PWD):/app -w /app -t --rm php:8.0-cli-alpine php -d memory_limit=-1 ./vendor/bin/phpstan analyse

.PHONY: format
format:
	docker run -v $(PWD):/app -w /app -t --rm php:8.0-cli-alpine php -d memory_limit=-1 ./vendor/bin/phpcbf
	docker run -v $(PWD):/app -w /app -t --rm php:8.0-cli-alpine php -d memory_limit=-1 ./vendor/bin/php-cs-fixer fix

.PHONY: prepare
prepare: vendor/autoload.php
	$(sy) doctrine:database:drop --if-exists --force
	$(sy) doctrine:database:create
	$(sy) doctrine:migrations:migrate -n
	$(sy) doctrine:fixture:load -n

vendor/autoload.php: composer.lock
	$(php) composer install
	touch vendor/autoload.php