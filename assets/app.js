/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

import './fontawesome/js/all';

import $ from 'jquery';
import { Popper } from '@popperjs/core';
import './plugins/bootstrap/js/bootstrap.js';
import './plugins/jquery-rss/dist/jquery.rss.min.js';
import './plugins/github-calendar/dist/github-calendar.min.js';
import './plugins/mustache.min.js';
import './plugins/github-activity/github-activity-0.1.5.min.js';

import './js/main.js';

