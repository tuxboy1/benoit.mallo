<?php

declare(strict_types=1);

namespace App\Security\Guard;

use App\Dto\Credentials;
use App\Form\LoginType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class WebAuthenticator extends AbstractFormLoginAuthenticator
{
    private const LOGIN_ROUTE_NAME = 'security_login';

    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private FormFactoryInterface $formFactory,
        private UserPasswordEncoderInterface $passwordEncoder,
    ) {
    }

    protected function getLoginUrl(): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE_NAME);
    }

    public function supports(Request $request): bool
    {
        return $request->isMethod(Request::METHOD_POST)
          && self::LOGIN_ROUTE_NAME === $request->attributes->get('_route');
    }

    public function getCredentials(Request $request): ?Credentials
    {
        $credentials = new Credentials();
        $form = $this->formFactory->create(LoginType::class, $credentials)->handleRequest($request);

        if (!$form->isValid()) {
            return null;
        }

        return $credentials;
    }

    /**
     * @param Credentials $credentials
     *
     * @return UserInterface|void|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $userProvider->loadUserByUsername($credentials->username ?? '');
    }

    /**
     * @param Credentials $credentials
     *
     * @return bool|void
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if ($this->passwordEncoder->isPasswordValid($user, $credentials->password ?? '')) {
            return true;
        }

        throw new AuthenticationException('Password not valid.');
    }

    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        string $providerKey
    ): RedirectResponse {
        return new RedirectResponse($this->urlGenerator->generate('home'));
    }
}
