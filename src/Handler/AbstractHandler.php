<?php

declare(strict_types=1);

namespace App\Handler;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractHandler implements HandlerInterface
{
    protected FormInterface $form;

    public function __construct(protected FormFactoryInterface $formFactory)
    {
    }

    abstract public function getDataTransfertObject(): object;

    abstract public function getFormType(): string;

    public function handle(Request $request): bool
    {
        $this->form = $this->formFactory
          ->create($this->getFormType(), $this->getDataTransfertObject())->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $this->process($this->form->getData());

            return true;
        }

        return false;
    }

    public function createView(): FormView
    {
        return $this->form->createView();
    }
}
