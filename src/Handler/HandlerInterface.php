<?php

declare(strict_types=1);

namespace App\Handler;

use Symfony\Component\Form\FormView;

interface HandlerInterface
{
    public function process(mixed $data): void;

    public function createView(): FormView;
}
