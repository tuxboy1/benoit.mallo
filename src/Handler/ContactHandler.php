<?php

declare(strict_types=1);

namespace App\Handler;

use App\Dto\ContactDto;
use App\Dto\Email;
use App\Form\ContactType;
use App\Services\SendInterface;
use Symfony\Component\Form\FormFactoryInterface;

class ContactHandler extends AbstractHandler
{
    public bool $hasError = false;

    public function __construct(
        protected FormFactoryInterface $formFactory,
        private SendInterface $sendService,
    ) {
    }

    /**
     * @param ContactDto $data
     */
    public function process(mixed $data): void
    {
        $email = new Email(
            to: 'benoit.mallo@gmail.com',
            from: $data->getEmail(),
            subject: 'Me contacter',
            content: $data->getMessage()
        );

        $this->hasError = false === $this->sendService->send($email);
    }

    public function getFormType(): string
    {
        return ContactType::class;
    }

    public function getDataTransfertObject(): object
    {
        return new ContactDto();
    }
}
