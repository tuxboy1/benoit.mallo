<?php

declare(strict_types=1);

namespace App\Handler\Security;

use App\Dto\Credentials;
use App\Form\LoginType;
use App\Handler\AbstractHandler;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginHandler extends AbstractHandler
{
    public function __construct(FormFactoryInterface $formFactory, private AuthenticationUtils $authenticationUtils)
    {
        $this->formFactory = $formFactory;
    }

    public function getDataTransfertObject(): object
    {
        return new Credentials();
    }

    public function getFormType(): string
    {
        return LoginType::class;
    }

    public function process(mixed $data): void
    {
    }

    public function handle(Request $request): bool
    {
        $this->form = $this->formFactory->create(
            $this->getFormType(),
            new Credentials($this->authenticationUtils->getLastUsername())
        );

        if (null !== $this->authenticationUtils->getLastAuthenticationError()) {
            $this->form
              ->addError(new FormError($this->authenticationUtils->getLastAuthenticationError()->getMessage()));

            return true;
        }

        return false;
    }
}
