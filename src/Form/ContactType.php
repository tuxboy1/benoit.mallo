<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface<\IteratorAggregate> $builder
     * @param string[]                                 $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, ['label' => 'Nom'])
            ->add('lastname', TextType::class, ['label' => 'Prénom'])
            ->add('email', EmailType::class, ['label' => 'Addresse email'])
            ->add('message', TextareaType::class, ['label' => 'Votre méssage'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
