<?php

namespace App\Services;

use App\Dto\Email;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email as EmailMime;

class SendService implements SendInterface
{
    public function __construct(private MailerInterface $mailer)
    {
    }

    public function send(Email $email): bool
    {
        try {
            $emailTransport = (new EmailMime())
              ->from($email->getFrom() ?? '')
              ->to($email->getTo())
              ->subject($email->getSubject())
              ->text($email->getContent() ?? '');

            $this->mailer->send($emailTransport);

            return true;
        } catch (TransportExceptionInterface $exception) {
            return false;
        }
    }
}
