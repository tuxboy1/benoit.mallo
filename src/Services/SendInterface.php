<?php

declare(strict_types=1);

namespace App\Services;

use App\Dto\Email;

interface SendInterface
{
    public function send(Email $email): bool;
}
