<?php

declare(strict_types=1);

namespace App\Dto;

class Email
{
    public function __construct(
        private string $to,
        private ?string $from,
        private string $subject,
        private ?string $content,
    ) {
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function getFrom(): ?string
    {
        return $this->from;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }
}
