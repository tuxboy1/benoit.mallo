<?php

declare(strict_types=1);

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ContactDto
{
    #[Assert\NotBlank]
    private ?string $firstname = null;

    #[Assert\NotBlank]
    private ?string $lastname = null;

    #[
        Assert\NotBlank,
        Assert\Email
    ]
    private ?string $email = null;

    #[
        Assert\NotBlank,
        Assert\Length(min: 5)
    ]
    private ?string $message = null;

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): ContactDto
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): ContactDto
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): ContactDto
    {
        $this->email = $email;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): ContactDto
    {
        $this->message = $message;

        return $this;
    }
}
