<?php

declare(strict_types=1);

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class Credentials
{
    #[
      Assert\NotBlank,
      Assert\Email
     ]
    public ?string $username = null;

    #[Assert\NotBlank]
    public ?string $password = null;

    public function __construct(?string $username = null)
    {
        $this->username = $username;
    }
}
