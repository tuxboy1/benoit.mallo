<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $levelArray = ['expert', 'pro', 'confirmé'];
        $faker = Factory::create('fr_FR');
        for ($i = 1; $i <= 10; ++$i) {
            $skill = (new Skill())
                ->setName($faker->name)
                ->setLevel($levelArray[$faker->randomKey(['expert', 'pro', 'confirmé'])])
                ->setPercentage($faker->randomFloat(2, 1, 100))
                ->setCreatedAt($faker->dateTime())
                ->setUpdatedAt($faker->dateTime())
            ;
            $manager->persist($skill);
        }

        $manager->flush();
    }
}
