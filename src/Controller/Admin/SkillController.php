<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin', name: 'admin_')]
class SkillController
{
    #[Route('/skill/add', name: 'skill_add')]
    public function add(): Response
    {
        return new Response('Test');
    }
}
