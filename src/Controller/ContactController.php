<?php

declare(strict_types=1);

namespace App\Controller;

use App\Handler\ContactHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

#[Route('/me-contacter', name: 'contact')]
class ContactController
{
    public function __construct(
        private Environment $twig,
        private UrlGeneratorInterface $urlGenerator,
        private ContactHandler $contactHandler,
    ) {
    }

    public function __invoke(Request $request): Response
    {
        if ($this->contactHandler->handle($request)) {
            /** @var FlashBag $flashBag */
            $flashBag = $request->getSession()->getBag('flashes');
            if ($this->contactHandler->hasError) {
                $flashBag->add('danger', 'Un problème est suvenu lors de l\'envoi de votre message');

                return new RedirectResponse($this->urlGenerator->generate('contact'));
            }

            $flashBag->add('success', 'Votre message a bien été envoyé. Merci.');

            return new RedirectResponse($this->urlGenerator->generate('contact'));
        }

        return new Response(
            $this->twig->render('contact/index.html.twig', ['form' => $this->contactHandler->createView()])
        );
    }
}
