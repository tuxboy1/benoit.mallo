<?php

namespace App\Controller;

use App\Handler\Security\LoginHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    public function __construct(private LoginHandler $loginHandler)
    {
    }

    #[Route('/login', name: 'security_login')]
    public function login(Request $request): Response
    {
        if ($this->loginHandler->handle($request)) {
            $this->addFlash('success', 'Vous êtes bien connecté.');
        }

        return $this->render('security/login.html.twig', ['form' => $this->loginHandler->createView()]);
    }

    #[Route('/logout', name: 'security_logout')]
    public function logout(): void
    {
    }
}
