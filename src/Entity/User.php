<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(unique=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(unique=true)
     */
    private ?string $pseudo = null;

    /**
     * @ORM\Column
     */
    private ?string $password = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $registerdAt;

    public function __construct()
    {
        $this->registerdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): User
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getRegisterdAt(): \DateTime | DateTimeInterface
    {
        return $this->registerdAt;
    }

    /**
     * @param DateTimeInterface $registerdAt
     */
    public function setRegisterdAt(\DateTime | DateTimeInterface $registerdAt): User
    {
        $this->registerdAt = $registerdAt;

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_ADMIN'];
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function eraseCredentials(): void
    {
    }
}
